package pl.edu.pwr.springdata.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwr.springdata.dao.OrderRepo;
import pl.edu.pwr.springdata.dao.entity.Order;

import java.util.Optional;

@Service
public class OrderManager {

    private OrderRepo orderRepo;

    @Autowired
    public OrderManager(OrderRepo orderRepo){
        this.orderRepo = orderRepo;
    }

    public Optional<Order> findById(Long id){
        return orderRepo.findById(id);
    }

    public Iterable<Order> findAll(){
        return orderRepo.findAll();
    }

    public Order save(Order order){
        return orderRepo.save(order);
    }

    public void deleteById(Long id){
        orderRepo.deleteById(id);
    }
}
