package pl.edu.pwr.springdata.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pwr.springdata.dao.entity.UserDTO;

@Repository
public interface UserDTORepo extends CrudRepository<UserDTO, Long> {
}
