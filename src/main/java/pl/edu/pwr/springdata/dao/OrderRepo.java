package pl.edu.pwr.springdata.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pwr.springdata.dao.entity.Order;

@Repository
public interface OrderRepo extends CrudRepository<Order, Long> {
}
