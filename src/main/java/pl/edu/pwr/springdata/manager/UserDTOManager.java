package pl.edu.pwr.springdata.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwr.springdata.dao.UserDTORepo;
import pl.edu.pwr.springdata.dao.entity.Customer;
import pl.edu.pwr.springdata.dao.entity.UserDTO;

import java.util.Optional;

@Service
public class UserDTOManager {
    private UserDTORepo userDTORepo;

    @Autowired
    public UserDTOManager(UserDTORepo userDTORepo){
        this.userDTORepo = userDTORepo;
    }

    public Optional<UserDTO> findById(Long id){
        return userDTORepo.findById(id);
    }

    public Iterable<UserDTO> findAll(){
        return userDTORepo.findAll();
    }

    public UserDTO save(UserDTO userDTO){
        return userDTORepo.save(userDTO);
    }

    public void deleteById(Long id){
        userDTORepo.deleteById(id);
    }
}
