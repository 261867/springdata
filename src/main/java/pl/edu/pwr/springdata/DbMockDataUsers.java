package pl.edu.pwr.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.edu.pwr.springdata.dao.UserDTORepo;
import pl.edu.pwr.springdata.dao.entity.UserDTO;

@Component
public class DbMockDataUsers {
    private UserDTORepo userDTORepo;

    @Autowired
    public DbMockDataUsers(UserDTORepo userDTORepo) {
        this.userDTORepo = userDTORepo;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() {
        PasswordEncoderConfig passwordEncoderConfig = new PasswordEncoderConfig();
        PasswordEncoder passwordEncoder = passwordEncoderConfig.passwordEncoder();
        UserDTOBuilder userDTOBuilder = new UserDTOBuilder(passwordEncoder);

        User userCustomer1 = new User("CustomerName", "hasloabc", "ROLE_CUSTOMER");
        User userAdmin1 = new User("AdminName", "haslo123", "ROLE_ADMIN");
        UserDTO userDTOCustomer1 = userDTOBuilder.createUser(userCustomer1);
        UserDTO userDTOAdmin1 = userDTOBuilder.createUser(userAdmin1);

        userDTORepo.save(userDTOCustomer1);
        userDTORepo.save(userDTOAdmin1);
    }
}
