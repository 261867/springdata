package pl.edu.pwr.springdata;

import org.springframework.security.crypto.password.PasswordEncoder;
import pl.edu.pwr.springdata.dao.entity.UserDTO;

/**
 * Klasa posiadająca metody do tworzenia obiektów klasy UserDTO na podsatwie obiektów klasy User
 */
public class UserDTOBuilder {
    private PasswordEncoder passwordEncoder;

    public UserDTOBuilder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public UserDTO createUser(User user) {
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        return new UserDTO(user.getName(), encodedPassword, user.getRole());
    }

}
