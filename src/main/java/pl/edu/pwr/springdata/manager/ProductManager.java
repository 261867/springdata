package pl.edu.pwr.springdata.manager;

//warstwa posrednia pomiedzy danymi a api

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwr.springdata.dao.ProductRepo;
import pl.edu.pwr.springdata.dao.entity.Product;

import java.util.Map;
import java.util.Optional;

@Service
public class ProductManager {
    private ProductRepo productRepo;

    @Autowired
    public ProductManager(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    public Optional<Product> findById(Long id){
        return productRepo.findById(id);
    }

    public Iterable<Product> findAll(){
        return productRepo.findAll();
    }

    public Product save(Product product){
        return productRepo.save(product);
    }

    public void deleteById(Long id){
        productRepo.deleteById(id);
    }

}
