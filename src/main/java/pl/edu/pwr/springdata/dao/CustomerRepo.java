package pl.edu.pwr.springdata.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pwr.springdata.dao.entity.Customer;

@Repository
public interface CustomerRepo extends CrudRepository<Customer, Long> {
}
