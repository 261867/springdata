package pl.edu.pwr.springdata.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwr.springdata.dao.entity.Order;
import pl.edu.pwr.springdata.manager.OrderManager;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class OrderApi {
    private OrderManager orders;

    @Autowired
    public OrderApi(OrderManager orders) {
        this.orders = orders;
    }

    @GetMapping("/order/all")
    public Iterable<Order> getAll(){
        return orders.findAll();
    }

    @GetMapping("/order")
    public Optional<Order> getById(@RequestParam Long id){
        return orders.findById(id);
    }

    @PostMapping("/order")
    public Order addOrder(@RequestBody Order order){
        return orders.save(order);
    }

    @PutMapping("/admin/order")
    public Order updateOrder(@RequestBody Order order){
        return orders.save(order);
    }

    @PatchMapping("/admin/order")
    public Order patchOrder(@RequestParam Long id, @RequestBody Map<String, Object> updates) {
        Order order = orders.findById(id).orElseThrow(() -> new IllegalArgumentException("Not found"));
        for (Map.Entry<String, Object> entry : updates.entrySet()) {
            String fieldName = entry.getKey();
            System.out.println(fieldName);
            Object value = entry.getValue();
            try {
                if (fieldName.equals("placeDate")) {
                    order.setPlaceDate((LocalDateTime) value);
                } else if (fieldName.equals("status")) {
                    order.setStatus((String) value);
                } else {
                    throw new IllegalArgumentException("Invalid field: " + fieldName);
                }
            } catch (Exception e) {
                // handle exception
                System.out.println(e);
            }
        }
        return orders.save(order);
    }

    @DeleteMapping("/admin/order")
    public void deleteOrder(@RequestParam Long id){
        orders.deleteById(id);
    }
}
