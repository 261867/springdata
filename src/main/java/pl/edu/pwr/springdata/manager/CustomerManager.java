package pl.edu.pwr.springdata.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwr.springdata.dao.CustomerRepo;
import pl.edu.pwr.springdata.dao.entity.Customer;


import java.util.Optional;

@Service
public class CustomerManager {
    private CustomerRepo customerRepo;

    @Autowired
    public CustomerManager(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    public Optional<Customer> findById(Long id){
        return customerRepo.findById(id);
    }

    public Iterable<Customer> findAll(){
        return customerRepo.findAll();
    }

    public Customer save(Customer customer){
        return customerRepo.save(customer);
    }

    public void deleteById(Long id){
        customerRepo.deleteById(id);
    }
}
