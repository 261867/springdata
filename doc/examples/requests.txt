PRODUCT
1) ADD
POST http://localhost:8080/api/product
{
    "name": "Monitor",
    "price": 900.0,
    "available": true
}

2) UPDATE
PUT http://localhost:8080/api/product
{
    "id": 3,
    "name": "Monitor",
    "price": 950.0,
    "available": true
}

3) PATCH
PATCH http://localhost:8080/api/product?id=3
{
    "available": false
}

4) DELETE
DELETE http://localhost:8080/api/product?id=4

5) GET BY ID
GET http://localhost:8080/api/product?id=3

6) GET ALL
GET http://localhost:8080/api/product/all

CUSTOMER
1) ADD
POST http://localhost:8080/api/customer
{
    "name": "Anna Jankowska",
    "address": "Toruń"
}

2) UPDATE
PUT http://localhost:8080/api/customer
{
    "id": 2,
    "name": "Anna Jankowiak",
    "address": "Tczew"
}

3) PATCH
PATCH http://localhost:8080/api/customer?id=1
{
    "name": "Jan Kowalski"
}

4) DELETE
DELETE http://localhost:8080/api/customer?id=4

5) GET BY ID
GET http://localhost:8080/api/customer?id=2

6) GET ALL
GET http://localhost:8080/api/customer/all

ORDER
1) ADD
POST http://localhost:8080/api/order
{
        "customer": {
            "id": 2,
            "name": "Anna Jankowiak",
            "address": "Tczew"
        },
        "products": [
            {
                "id": 2,
                "name": "Rura",
                "price": 5.0,
                "available": true
            }
        ],
        "placeDate": "2023-04-29T13:33:19.478186",
        "status": "payment processing"
    }

2) UPDATE
PUT http://localhost:8080/api/order
{
        "customer": {
            "id": 2,
            "name": "Anna Jankowiak",
            "address": "Tczew"
        },
        "products": [
            {
                "id": 2,
                "name": "Rura",
                "price": 5.0,
                "available": true
            }
        ],
        "placeDate": "2023-04-29T13:33:19.478186",
        "status": "delivered"
    }

3) PATCH
PATCH http://localhost:8080/api/order?id=5
{
    "status": "cancelled"
}

4) DELETE
DELETE http://localhost:8080/api/order?id=5

5) GET BY ID
GET http://localhost:8080/api/order?id=3

6) GET ALL
GET http://localhost:8080/api/order/all






