package pl.edu.pwr.springdata.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwr.springdata.dao.entity.Product;
import pl.edu.pwr.springdata.manager.ProductManager;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductApi {

    private ProductManager products;

    @Autowired
    public ProductApi(ProductManager products) {
        this.products = products;
    }

    @GetMapping("/product/all")
    public Iterable<Product> getAll(){
        return products.findAll();
    }

    @GetMapping("/product")
    public Optional<Product> getById(@RequestParam Long id){
        return products.findById(id);
    }

    @PostMapping("/admin/product")
    public Product addProduct(@RequestBody Product product){
        return products.save(product);
    }

    @PutMapping("/admin/product")
    public Product updateProduct(@RequestBody Product product){
        return products.save(product);
    }

    @PatchMapping("/admin/product")
    public Product patchProduct(@RequestParam Long id, @RequestBody Map<String, Object> updates) {
        Product product = products.findById(id).orElseThrow(() -> new IllegalArgumentException("Not found"));
        System.out.println(product);
        for (Map.Entry<String, Object> entry : updates.entrySet()) {
            String fieldName = entry.getKey();
            System.out.println(fieldName);
            Object value = entry.getValue();
            try {
                if (fieldName.equals("name")) {
                    product.setName((String) value);
                } else if (fieldName.equals("price")) {
                    product.setPrice(Float.valueOf(String.valueOf(value)));
                    System.out.println("aaa!!!");
                } else if (fieldName.equals("available")) {
                    product.setAvailable((boolean) value);
                } else {
                    throw new IllegalArgumentException("Invalid field: " + fieldName);
                }
            } catch (Exception e) {
                // handle exception
                System.out.println(e);
            }
        }
        return products.save(product);
    }

    @DeleteMapping("/admin/product")
    public void deleteProduct(@RequestParam Long id){
        products.deleteById(id);
    }
}
