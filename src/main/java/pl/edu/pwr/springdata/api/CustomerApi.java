package pl.edu.pwr.springdata.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwr.springdata.dao.entity.Customer;
import pl.edu.pwr.springdata.manager.CustomerManager;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CustomerApi {
    private CustomerManager customers;

    @Autowired
    public CustomerApi(CustomerManager customers) {
        this.customers = customers;
    }

    @GetMapping("/customer/all")
    public Iterable<Customer> getAll(){
        return customers.findAll();
    }

    @GetMapping("/customer")
    public Optional<Customer> getById(@RequestParam Long id){
        return customers.findById(id);
    }

    @PostMapping("/admin/customer")
    public Customer addCustomer(@RequestBody Customer customer){
        return customers.save(customer);
    }

    @PutMapping("/admin/customer")
    public Customer updateCustomer(@RequestBody Customer customer){
        return customers.save(customer);
    }

    @PatchMapping("/admin/customer")
    public Customer patchCustomer(@RequestParam Long id, @RequestBody Map<String, Object> updates) {
        Customer customer = customers.findById(id).orElseThrow(() -> new IllegalArgumentException("Not found"));
        for (Map.Entry<String, Object> entry : updates.entrySet()) {
            String fieldName = entry.getKey();
            System.out.println(fieldName);
            Object value = entry.getValue();
            try {
                if (fieldName.equals("name")) {
                    customer.setName((String) value);
                } else if (fieldName.equals("address")) {
                    customer.setAddress((String) value);
                } else {
                    throw new IllegalArgumentException("Invalid field: " + fieldName);
                }
            } catch (Exception e) {
                // handle exception
                System.out.println(e);
            }
        }
        return customers.save(customer);
    }

    @DeleteMapping("/admin/customer")
    public void deleteCustomer(@RequestParam Long id){
        customers.deleteById(id);
    }
}
