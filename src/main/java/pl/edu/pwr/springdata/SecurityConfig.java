package pl.edu.pwr.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    private final DataSource dataSource;
    @Autowired
    private final PasswordEncoder passwordEncoder;

    public SecurityConfig(DataSource dataSource, PasswordEncoder passwordEncoder) {
        this.dataSource = dataSource;
        this.passwordEncoder = passwordEncoder;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
        http.authorizeHttpRequests()
                .requestMatchers(HttpMethod.GET, "/api/product").hasAnyRole("CUSTOMER", "ADMIN")
                .requestMatchers(HttpMethod.GET, "/api/product/all").hasAnyRole("CUSTOMER", "ADMIN")
                .requestMatchers(HttpMethod.GET, "/api/order").hasAnyRole("CUSTOMER", "ADMIN")
                .requestMatchers(HttpMethod.GET, "/api/order/all").hasAnyRole("CUSTOMER", "ADMIN")
                .requestMatchers(HttpMethod.POST,  "api/order").hasAnyRole("CUSTOMER", "ADMIN")

                .requestMatchers(HttpMethod.GET,  "api/customer").hasRole("CUSTOMER")
                .requestMatchers(HttpMethod.GET,  "api/customer/all").hasRole("CUSTOMER")

                .requestMatchers(HttpMethod.POST, "api/admin/product").hasRole("ADMIN")
                .requestMatchers(HttpMethod.PUT, "api/admin/product").hasRole("ADMIN")
                .requestMatchers(HttpMethod.PATCH, "api/admin/product").hasRole("ADMIN")
                .requestMatchers(HttpMethod.POST, "api/admin/customer").hasRole("ADMIN")
                .requestMatchers(HttpMethod.PUT, "api/admin/customer").hasRole("ADMIN")
                .requestMatchers(HttpMethod.PATCH, "api/admin/customer").hasRole("ADMIN")
                .requestMatchers(HttpMethod.PUT, "api/admin/order").hasRole("ADMIN")
                .requestMatchers(HttpMethod.PATCH, "api/admin/order").hasRole("ADMIN")


                .and()
                .formLogin().permitAll()
                .and()
                .logout().permitAll();
        return http.build();
    }

    @Primary
    @Bean
    public AuthenticationManagerBuilder configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .usersByUsernameQuery("SELECT u.name, u.password_hash, 1 FROM userdto u WHERE u.name=?")
                .authoritiesByUsernameQuery("SELECT u.name, u.role, 1 FROM userdto u WHERE u.name=?")
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder);
        return auth;
    }
}
